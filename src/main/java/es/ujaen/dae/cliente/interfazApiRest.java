/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.cliente;

import es.ujaen.dae.entidadesDTO.EnvioDTO;
import es.ujaen.dae.entidadesDTO.clienteDTO;
import excepciones.errorInterfaz;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import excepciones.Pair;

/**
 *
 * @author jose7
 */
@Component
public class interfazApiRest {

    HttpClient httpCliente;
    ClientHttpRequestFactory reqFactory;
    RestTemplate restTemplate;

    private String dni;


    public interfazApiRest(){
        httpCliente = HttpClientBuilder.create().build();
        reqFactory = new HttpComponentsClientHttpRequestFactory(httpCliente);
        restTemplate = new RestTemplate(reqFactory);
    }
    public interfazApiRest(String dni, String contraseña) {
        this.dni = dni;
        CredentialsProvider provider = new BasicCredentialsProvider();
        UsernamePasswordCredentials credentials
                = new UsernamePasswordCredentials(dni, contraseña);
        provider.setCredentials(AuthScope.ANY, credentials);

        httpCliente = HttpClientBuilder.create().build();
//        httpCliente = HttpClientBuilder.create().setDefaultCredentialsProvider(provider).build();
        reqFactory = new HttpComponentsClientHttpRequestFactory(httpCliente);

        restTemplate = new RestTemplate(reqFactory);
    }



    public Pair<errorInterfaz, EnvioDTO> detalleEnvio(int localizador) {
        String url = "http://localhost:8080/ujapack/envio/{localizador}";
        ResponseEntity<EnvioDTO> response = null;

//        try {

            response = restTemplate.getForEntity(url, EnvioDTO.class, localizador);

//        } catch (Exception ex) {
//
//            if (ex instanceof org.springframework.web.client.HttpClientErrorException && ex.getMessage().startsWith("404")) {
//                return new Pair<>(errorInterfaz.ErrorCredenciales, null);
//            } else {
//                return new Pair<>(errorInterfaz.ErrorConexion, null);
//            }
//        }

        return new Pair<>(errorInterfaz.OK, response.getBody());
    }

    public Pair<errorInterfaz, EnvioDTO> avanzaPaquete(int localizador) {
        String url = "http://localhost:8080/ujapack/envio/{localizador}";
        ResponseEntity<EnvioDTO> response = null;

        try {

            restTemplate.put(url, EnvioDTO.class, localizador);

            response = restTemplate.getForEntity(url, EnvioDTO.class, localizador);

        } catch (Exception ex) {

            if (ex instanceof org.springframework.web.client.HttpClientErrorException && ex.getMessage().startsWith("404")) {
                return new Pair<>(errorInterfaz.ErrorCredenciales, null);
            } else {
                return new Pair<>(errorInterfaz.ErrorConexion, null);
            }
        }

        return new Pair<>(errorInterfaz.OK, response.getBody());
    }

    
    
    public Pair<errorInterfaz, EnvioDTO> creaEnvio(EnvioDTO envio) {
        String url = "http://localhost:8080/ujapack/envio";
        ResponseEntity<EnvioDTO> response = null;

        try {
            response = restTemplate.postForEntity(url,envio ,EnvioDTO.class);

        } catch (Exception ex) {

            if (ex instanceof org.springframework.web.client.HttpClientErrorException && ex.getMessage().startsWith("404")) {
                return new Pair<>(errorInterfaz.ErrorCredenciales, null);
            } else {
                return new Pair<>(errorInterfaz.ErrorConexion, null);
            }
        }

        return new Pair<>(errorInterfaz.OK, response.getBody());
    }
}
