package es.ujaen.dae.cliente;

import es.ujaen.dae.entidadesDTO.EnvioDTO;
import es.ujaen.dae.entidadesDTO.clienteDTO;
import excepciones.Pair;
import excepciones.errorInterfaz;
import java.util.Scanner;

public class UjapackCliente {
    private interfazApiRest interfaz;
    clienteDTO cliente;
    EnvioDTO envio;
    Scanner scanner = new Scanner(System.in);

    public void limpiarConsola() {

        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");
        System.out.println("");


    }
    public void menuInicial(){
        System.out.println("Bienvenido al Menu Inicial de UjaPack");

        int opcion = 0;

        do {
            limpiarConsola();
            System.out.println("Acciones:");
            System.out.println("1->Localizar Envio.");
            System.out.println("2->Acceder con credenciales.");
            System.out.println("3->Salir");
            System.out.println("Elija una opción:");
            interfaz = new interfazApiRest();
            try {
                opcion = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                opcion = 0;
            }

            switch (opcion) {
                case 1:
                    menuDetalleEnvio();
                    break;

                case 2:
                    menuLogin();
                    break;
                case 3:

                    break;
                default:
                    System.out.println("Opción no válida");
                    System.out.println("");
                    System.out.println("");
            }

        } while (opcion != 3);

    }
    
    public void menuLogin() {

        Pair<errorInterfaz, clienteDTO> respuesta;
        System.out.println("Bienvenido al Menu de UjaPack");

//        do {
            System.out.println("Por favor escriba su DNI (incluyendo la letra): ");
            String dni = scanner.nextLine();

            System.out.println("Y su contraseña:");
            String contraseña = scanner.nextLine();

            System.out.println("\nComprobando credenciales...");
            interfaz = new interfazApiRest(dni, contraseña);

//            respuesta = interfaz.comprobarConexion();
            limpiarConsola();
//
//            if (respuesta.first == errorInterfaz.ErrorCredenciales) {
//                System.out.println("Credenciales incorrectas");
//            }
//
//            if (respuesta.first == errorInterfaz.ErrorConexion) {
//                System.out.println("Error conexion");
//            }

//        } while (respuesta.first != errorInterfaz.OK);
//
//        cliente = respuesta.second;
//
//        System.out.println("Login correcto");

        menu();

    }
    
    private void menu() {

        int opcion = 0;

        do {
            limpiarConsola();
            System.out.println("Acciones:");
            System.out.println("1->Detalle del envio.");
            System.out.println("2->Avanza Paquete.");
            System.out.println("3->Crear envio.");
            System.out.println("4->Salir");
            System.out.println("Elija una opción:");

            try {
                opcion = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                opcion = 0;
            }

            switch (opcion) {
                case 1:
                    menuDetalleEnvio();
                    break;

                case 2:
                    menuAvanzaPaquete();
                    break;

                case 3:
                    menuCreaEnvio();
                    break;

                case 4:
                    System.out.println("Adios.");
                    break;

                default:
                    System.out.println("Opción no válida");
                    System.out.println("");
                    System.out.println("");
            }

        } while (opcion != 4);
    }
    
    private void menuDetalleEnvio() {

        int opcion = 0;
        int localizador;
        limpiarConsola();
        System.out.println("Introduce el localizador del paquete que quieres avanzar");
        localizador = Integer.parseInt(scanner.nextLine());
        
//        if(localizador < 0)
//            errorInterfaz.ErrorFormatoDatos;
            
        
        //Comprobar datos introducidos antes de enviarlos al servidor
        Pair<errorInterfaz, EnvioDTO> respuesta = interfaz.detalleEnvio(localizador);

        if (respuesta.first == errorInterfaz.OK) {

            envio = respuesta.second;
            
            System.out.println(envio);
        }
        
        

        do {
            
            
            System.out.println("Acciones:");            
            System.out.println("1->Menu inicial");
            System.out.println("2->Salir");
            System.out.println("Elija una opción:");
            

//            try {
                opcion = Integer.parseInt(scanner.nextLine());
//            } catch (NumberFormatException e) {
//                opcion = 0;
//            }

            switch (opcion){
                case 1:
                    menuInicial();
                    break;
                case 2:
                    break;
                
                default:
                    System.out.println("Opción no válida");
                    System.out.println("");
                    System.out.println("");
            }

        } while (opcion != 2);

    }
    
    
    private void menuAvanzaPaquete() {
        int opcion = 0;
        limpiarConsola();
        int localizador;
        System.out.println("Menu Avanza Paquete");
        
        System.out.println("Introduce el localizador del paquete que quieres avanzar");
        localizador = Integer.parseInt(scanner.nextLine());
        
//        if(localizador < 0)
//            errorInterfaz.ErrorFormatoDatos;
            
        
        //Comprobar datos introducidos antes de enviarlos al servidor
        Pair<errorInterfaz, EnvioDTO> respuesta = interfaz.avanzaPaquete(localizador);

        if (respuesta.first == errorInterfaz.OK) {

            envio = respuesta.second;

        }
        do{
        System.out.println("Acciones:");
        System.out.println("1->Menu inicial");
            System.out.println("2->Salir");
            System.out.println("Elija una opción:");
            

            try {
                opcion = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                opcion = 0;
            }

            switch (opcion){
                case 1:
                    menu();
                    break;
                case 2:
                    break;
                default:
                    System.out.println("Opción no válida");
                    System.out.println("");
                    System.out.println("");
            }
            

        } while (opcion != 2);

    }
    
    private void menuCreaEnvio() {
        int opcion = 0;
        limpiarConsola();
        envio= new EnvioDTO();
        
        System.out.println("Menu para crear un nuevo envio");
        
        System.out.println("Introduce el destinatario:");
        envio.setDestinatario(scanner.nextLine());
        System.out.println("Introduce el el remitente");
        envio.setRemitente(scanner.nextLine());
        System.out.println("Introduce el id de la oficina destino:");
        envio.setIdOficinaDestino(Integer.parseInt(scanner.nextLine()));
        System.out.println("Introduce el id de la oficina origen:");
        envio.setIdOficinaOrigen(Integer.parseInt(scanner.nextLine()));
        System.out.println("Introduce el peso del paquete:");
        envio.setPeso(Float.parseFloat(scanner.nextLine()));
        //--------------
        System.out.println("Introduce las dimensiones del paquete:");
        envio.setDimensiones(Double.parseDouble(scanner.nextLine()));

        Pair<errorInterfaz, EnvioDTO> respuesta = interfaz.creaEnvio(envio);

        if (respuesta.first == errorInterfaz.OK) {

            envio = respuesta.second;

        }
        do{
        System.out.println("Acciones:");
            System.out.println("1->Menu inicial");
            System.out.println("2->Salir");

            System.out.println("Elija una opción:");
            

            try {
                opcion = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                opcion = 0;
            }

            switch (opcion){
                case 1:
                    menu();
                    break;
                case 2:
                    break;
                default:
                    System.out.println("Opción no válida");
                    System.out.println("");
                    System.out.println("");
            }
            

        } while (opcion != 2);

    }
    
    
    

    
}
