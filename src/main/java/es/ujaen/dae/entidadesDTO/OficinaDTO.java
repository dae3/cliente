package es.ujaen.dae.entidadesDTO;

public class OficinaDTO {

    private int id;
    private String nombre;
    private int idCLPertenece;

    public OficinaDTO(){

    }

    public OficinaDTO(int id, String nombre, int idCLPertenece) {
        this.id = id;
        this.nombre = nombre;
        this.idCLPertenece = idCLPertenece;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public int getIdCLPertenece() {
        return idCLPertenece;
    }
}
