package es.ujaen.dae.entidadesDTO;

import java.time.LocalDateTime;

public class PuntodeControlDTO {
    private int id;

    private LocalDateTime fechaSalida;
    private LocalDateTime fechaEntrada;

    private int idPertenece;
    private boolean isCentrologistico;
    private boolean activo;

    public PuntodeControlDTO(){

    }

    public PuntodeControlDTO(int id, LocalDateTime fechaSalida, LocalDateTime fechaEntrada, int idPertenece, boolean activo,boolean isCentrologistico) {
        this.id = id;
        this.fechaSalida = fechaSalida;
        this.fechaEntrada = fechaEntrada;
        this.idPertenece = idPertenece;
        this.activo = activo;
        this.isCentrologistico = isCentrologistico;
    }

    public int getId() {
        return id;
    }

    public LocalDateTime getFechaSalida() {
        return fechaSalida;
    }

    public LocalDateTime getFechaEntrada() {
        return fechaEntrada;
    }

    public int getIdPertenece() {
        return idPertenece;
    }

    public boolean isCentrologistico() {
        return isCentrologistico;
    }

    public boolean isActivo() {
        return activo;
    }

    @Override
    public String toString() {
        return "PuntodeControlDTO{" + "id=" + id + ", fechaSalida=" + fechaSalida + ", fechaEntrada=" + fechaEntrada + '}';
    }
    
    
    

}
