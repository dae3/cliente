/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.ujaen.dae.entidadesDTO;

/**
 *
 * @author jose7
 */
public class clienteDTO {

    public enum Rol {
        repartidor,
        oficinista
    }

    private String dni;
    private String nombre;
    private String rol;
    private String contraseña;


    public clienteDTO(){

    }

    public clienteDTO(String dni, String nombre,String rol) {
        this.dni = dni;
        this.nombre = nombre;
        this.rol=rol;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
}

