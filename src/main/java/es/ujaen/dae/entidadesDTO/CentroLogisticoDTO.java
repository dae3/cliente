package es.ujaen.dae.entidadesDTO;

import org.springframework.hateoas.Link;

import java.util.List;

public class CentroLogisticoDTO {

    private int id;
    private String nombre;
    private String localizacion;

    //private List<CentroLogistico> conexiones= new ArrayList<>();
    private List<Link> conexiones;
    //private Map<Integer,Oficina> oficinas = new HashMap<>();
    private List<Link> oficinas;


    public CentroLogisticoDTO() {

    }

    public CentroLogisticoDTO(int id, String nombre, String localizacion) {
        this.id = id;
        this.nombre = nombre;
        this.localizacion = localizacion;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getLocalizacion() {
        return localizacion;
    }
}
