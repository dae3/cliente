package es.ujaen.dae.entidadesDTO;


import org.springframework.hateoas.Link;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class EnvioDTO {

    public enum Estado {
        enTransito,
        enReparto,
        entregado
    };

    private int localizador;

    private int idOficinaDestino;
    private int idOficinaOrigen;
    private String destinatario;
    private String remitente;
    private float peso;

    private double dimensiones;
    private Estado estado;
    private LocalDateTime fechaEntrega;
    private double importe;
    private float alto;
    private float ancho;
    private float largo;

    //public List<PuntoDeControl> ruta;
    private List<Link> ruta;
    public EnvioDTO (){};
    public EnvioDTO(String remitente,String destinatario,float peso,
                     float alto, float ancho, float largo,
                    int idOficinaDestino, int idOficinaOrigen) {
        this.idOficinaDestino = idOficinaDestino;
        this.idOficinaOrigen = idOficinaOrigen;
        this.destinatario = destinatario;
        this.remitente = remitente;
        this.peso = peso;
        this.alto = alto;
        this.ancho = ancho;
        this.largo = largo;
        }

    public int getLocalizador() {
        return localizador;
    }

    public int getIdOficinaDestino() {
        return idOficinaDestino;
    }

    public int getIdOficinaOrigen() {
        return idOficinaOrigen;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public String getRemitente() {
        return remitente;
    }

    public float getPeso() {
        return peso;
    }

    public double getDimensiones() {
        return dimensiones;
    }

    public Estado getEstado() {
        return estado;
    }

    public LocalDateTime getFechaEntrega() {
        return fechaEntrega;
    }

    public double getImporte() {
        return importe;
    }

    public double calcularImporte(float peso, double dimensiones, int tamRuta) {
        importe = peso * dimensiones * (tamRuta + 1) / 1000;
        return importe;
    }

    public List<Link> getRuta() {
        return ruta;
    }

    public void añadirRuta(Link pC){
        ruta.add(pC);
    }

    public void setLocalizador(int localizador) {
        this.localizador = localizador;
    }

    public void setIdOficinaDestino(int idOficinaDestino) {
        this.idOficinaDestino = idOficinaDestino;
    }

    public void setIdOficinaOrigen(int idOficinaOrigen) {
        this.idOficinaOrigen = idOficinaOrigen;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public void setRemitente(String remitente) {
        this.remitente = remitente;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public void setDimensiones(double dimensiones) {
        this.dimensiones = dimensiones;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public void setFechaEntrega(LocalDateTime fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public void setAlto(float alto) {
        this.alto = alto;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }

    public void setRuta(List<Link> ruta) {
        this.ruta = ruta;
    }

    @Override
    public String toString() {
        return "EnvioDTO{" + "localizador=" + localizador + ", remitente=" + remitente + ", estado=" + estado + ", fechaEntrega=" + fechaEntrega + ", ruta=" + ruta + '}';
    }
    
    
    
}
